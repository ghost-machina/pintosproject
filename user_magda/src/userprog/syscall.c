#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"

static void syscall_handler (struct intr_frame *);
static struct lock *lock_files; /* syncronize file processing */
static struct list all_files_list;
static struct file_elem *find_file(int);
static int global_fid = 2;


void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&lock_files);
  list_init(&all_files_list);
}

static int open(const char*  name)
{
	struct file* file;
	struct file_elem *f;
	int fid = global_fid++;

	f = (file_elem*)malloc(sizeof(file_elem));

	if(!is_user_vaddr(name))
	{
		return -1;
	}

	lock_aquire(&lock_files);
	file = filesys_open(name);
	f->file = file;
	f->fid = fid;
	list_push_back(&current_thread()->files, &f->thread_elem);
	list_push_back(&all_files_list,&f->global_elem);
	lock_release(&lock_files);
	return fid;
}

static int filesize(int fid)
{
	struct file_elem *f = find_file(fid);

	if (f == NULL)
		return -1;
	else
		return file_length(f->file);
}

static int read(int fid, void *buffer, unsigned size)
{
	int return_value,i;
	struct file_elem *f;

	if (!is_user_vaddr(buffer) || !is_user_vaddr(buffer+size))
	{
		return -1;
	}
	lock_acquire(&lock_files);

	if(fid == 0)
	{
		for(i=0;i<size;i++)
		{
			*((uint8_t*)buffer+i)=input_getc();
		}
		return_value = size;
	}
	else if(fid == 1)
	{
		return_value = -1;
	}
	else
	{
		f = find_file(fid);
		if (f == NULL)
		{
			lock_release(&lock_files);
			return -1;
		}

		if(file_tell(f->file) + size > file_length(f->file))
		{
			return_value = file_length(f->file) - file_tell(f->file);
		}
		else
		{
			return_value = size;
		}
		file_read(f,buffer,return_value);
	}

	lock_release(&lock_files);
	return return_value;
}

static int write(int fid, const void *buffer, unsigned size)
{
	int return_value;
	struct file_elem *f;

	if(!is_user_vaddr(buffer) || !is_user_vaddr(buffer+size))
	{
		return -1;
	}

	lock_acquire(&lock_files);
	if (fid == 1) /*console*/
	{
			putbuf(buffer,size);
			return_value = (int)size;
	}
	else if(fid == 0)
	{
		return_value = -1;
	}
	else
	{
		f = find_file(fid);
		if (f == NULL)
		{
			lock_release(&lock_files);
			return -1;
		}

		if(file_tell(f->file) + size > file_length(f->file))
		{
			return_value = file_length(f->file) - file_tell(f->file);
		}
		else
		{
			return_value = size;
		}
		file_write(f,buffer,return_value);
	}
	lock_release(&lock_files);
	return return_value;
}

static void seek(int fid, unsigned position)
{
	int newPos;
	struct file_elem *f = find_file(fid);

	if (f==NULL)
		return -1;

	if (position < 0 || position > file_length(f->file))
		return -1;

	file_seek(f->file,position);
}

static unsigned tell(int fid)
{
	struct file_elem *f = find_file(fid);

	if (f == NULL)
		return -1;

	return file_tell(f->file);
}

static void close(int fid)
{
	struct file_elem *f = find_file(fid);

	if(f != NULL)
	{
		file_close(f->file);
		list_remove(&f->global_elem);
		list_remove(&f->thread_elem);

		free(f);
	}
}

static struct file_elem *find_file(int fid)
{
	struct list_elem  *f_elem = list_begin(&all_files_list);

	while(f_elem != list_tail(&all_files_list))
	{
		struct file_elem *file = list_entry (f_elem, struct file_elem, global_elem);
		if(file->fid == fid)
		{
			return file;
		}
		f_elem = f_elem->next;
	}

	return NULL;
}

static void
syscall_handler (struct intr_frame *f)
{
  //printf ("system call!\n");
  //thread_exit ();
	int fid;
	void *buffer;
	char *cbuffer;
	unsigned pos;
	int syscall_no = ((int *)f->esp)[0];

	switch(syscall_no){
		case SYS_OPEN:
			//f->eax = open(const char* file)
			cbuffer = (char *)((int *)f->esp)[1];
			f->eax = open(cbuffer);
			break;
		case SYS_FILESIZE:
			//f->eax = filesize(int fid)
			fid = (int)((int *)f->esp)[1];
			f->eax = filesize(fid);
			break;
		case SYS_READ:
			//f->eax = read(int,void,unsigned)
			fid = (int)((int *)f->esp)[1];
			buffer = (void *)((int *)f->esp)[2];
			pos = (int)((int *)f->esp)[3];
			f->eax = read(fid,buffer,pos);
			break;
		case SYS_WRITE:
			//f->eax = write(int,const void *buffer, unsigned size)
			fid = (int)((int *)f->esp)[1];
			buffer = (void *)((int *)f->esp)[2];
			pos = (int)((int *)f->esp)[3];
			f->eax = write(fid,buffer,pos);
			break;
		case SYS_SEEK:
			//seek(int fid,unsigned position)
			fid = (int)((int *)f->esp)[1];
			pos = (int)((int *)f->esp)[2];
			seek(fid,pos);
			break;
		case SYS_TELL:
			//f->eax = tell(int);
			fid = (int)((int *)f->esp)[1];
			f->eax=tell(fid);
			break;
		case SYS_CLOSE:
			//close(int);
			fid = (int)((int *)f->esp)[1];
			close(fid);
			break;
		default:
		    NOT_REACHED ();
	}
}
