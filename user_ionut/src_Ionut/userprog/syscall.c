#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
static void syscall_handler (struct intr_frame *);
static struct lock file_exec_lock;
void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f )
{
	struct file_elem *f;
	int syscall_no =((int*)f->esp)[0];
	int arguments[127];
	int *ptr_data;
	bool are_waiting = false;

  switch(syscall_no) {
  /*
  case SYS_EXIT:
	  printf("SYS CALL EXIT!");
	  struct thread *current = thread_current();

	  thread_exit();
	  break;
  case SYS_WRITE:
	  printf("SYS CALL WRITE");
	  f->eax =0;
	  return;
	  break;
	  */
  case SYS_HALT:
	  printf("SYS CALL HALT");
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 		 shutdown_power_off();
	 	  }
	  break;

  case SYS_EXIT:
	  printf("SYS CALL EXIT!");

	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 		  arguments[0] = *ptr_data;
	 	  }
	 	 struct thread *current_thread = thread_current();
	 	 struct thread *child_iterator_thread;
	 	 struct list_elem *listEl;
	 	 // ne uitam sa vedem daca are procese care asteapta
	 	for (listEl = list_begin (&current_thread->children_processes); listEl != list_end (&current_thread->children_processes); listEl = list_next (listEl))
	 	{
	 		child_iterator_thread =  list_entry (listEl, struct thread, child);
	 		if (child_iterator_thread->status == THREAD_BLOCKED)
	 		{
	 			thread_unblock (child_iterator_thread); //altfel e deadlock
	 			are_waiting = true;
	 		}
	 		else
	 		{
	 		child_iterator_thread->parent_process = NULL;
	 		list_remove (&child_iterator_thread->child);
	 		}
	 	}


/*
	 	 while(!list_empty(&current_thread->children_processes))
	 	 {
				 child_iterator_thread = list_begin(&current_thread->children_processes);

				 if (child_iterator_thread->status == THREAD_BLOCKED)
					 thread_unblock (child_iterator_thread);
				else
				{

				list_remove (&child_iterator_thread,);
				}
				 if (thread_alive(&child_iterator_thread))
				 {
					 are_waiting = true;
				 }
	 	 }
*/
	 	//ne uitam sa inchided toate fisierele deschise penru thread-ul current altfel ar
	 	 // putea duce la erori neprevazute
	 	while (!list_empty (&current_thread->list_of_files))
	 	{
	 	listEl = list_begin (&current_thread->list_of_files);
	 	 //aici apelam close file.
	 	}

	 	if (are_waiting)
	 	{
		 	printf("Thread Name %s has exited with exit code %d. Has waiting children", current_thread->name, current_thread->status);
	 	}
	 	else
	 	{
		 	printf("Thread Name %s has exited with exit code %d. Has no waiting children", current_thread->name, current_thread->status);
	 	}
	 	thread_exit();
	  break;
  case SYS_WAIT:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	  if (is_user_vaddr((const void *) ptr_data)) {
		  arguments[0] = *ptr_data;
	  }
	 int return_code_from_wait = process_wait(arguments[0]); //apelam un process wait dupa process id
	 f->eax = return_code_from_wait;
	  break;

  case SYS_EXEC:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	  	  if (is_user_vaddr((const void *) ptr_data)) {
	  		void *ptr_page = pagedir_get_page(thread_current()->pagedir, ptr_data);
	  		if (ptr_page)
	  		{
	  			arguments[0] = *ptr_data;
	  		}
	  		else
	  		{
	  			//error ar trebui sa iesim cumva de aici ca inseamna
	  			// ca nu toate elementele au fost ok e ceva gen adresa de baza + buffers length
	  			// din cate am inteles din documentatia pintos.
	  		}
	  	  }

	  int return_code_from_exec ;
	  lock_acquire (&file_exec_lock);
	  return_code_from_exec = process_execute (arguments[0]);
	  lock_release (&file_exec_lock);
	  f->eax = return_code_from_exec;
	  break;

  case SYS_CREATE:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 		  arguments[0] = *ptr_data;
	 	  }
	 	 ptr_data = (int *) f->esp + 2; //luam de pe stiva arg[1],fiind numele fisierului, si verificam daca e pointer valid
	 		  if (is_user_vaddr((const void *) ptr_data)) {
	 			  arguments[1] = *ptr_data;
	     }
	 	int return_code_from_create_file;
	 	lock_acquire(&file_exec_lock);
	 	return_code_from_create_file = filesys_create((const char *)arguments[0], (unsigned) arguments[1]);
	 	lock_release(&file_exec_lock);
	 	f->eax = return_code_from_create_file;
	  break;
  case SYS_REMOVE:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 	 		  arguments[0] = *ptr_data;
	 	 	  }

	 	 	int return_code_from_delete_file;
	 	 	lock_acquire(&file_exec_lock);
	 	 	return_code_from_delete_file = filesys_remove((const char *)arguments[0]);
	 	 	lock_release(&file_exec_lock);
	 	 	f->eax = return_code_from_delete_file;
	  break;
  }

}
