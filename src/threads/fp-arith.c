/*
 * fp-arith.c
 *
 *  Created on: Dec 18, 2015
 *      Author: osd
 */

#include <inttypes.h>
#include "fp-arith.h"

//const int64_t FIXED_POINT = 1 << 14;

/*Conversion functions here*/
//
//int64_t to_fp_from_integer (int64_t number)
//{
//	return number * FIXED_POINT;
//}
//
//int64_t to_integer_from_fp_trunc(int64_t number)
//{
//	return number / FIXED_POINT;
//}
//
//int64_t to_integer_from_fp_round(int64_t number)
//{ // hope i didn't get the ternary wrong lol
//	return (number>0) ? ((number+FIXED_POINT/2)/FIXED_POINT) : ((number-FIXED_POINT/2)/FIXED_POINT);
//}
//
///*Addition and subtraction here!*/
//
//int64_t fp_add(int64_t number1, int64_t number2)
//{
//	return number1+number2;
//}
//
//int64_t fp_add_int(int64_t number1, int64_t number2)
//{
//	return number1+to_fp_from_integer(number2);
//}
//
//int64_t fp_sub(int64_t number1, int64_t number2)
//{
//	return number1-number2;
//}
//
//int64_t fp_sub_int(int64_t number1, int64_t number2)
//{
//	return number1-to_fp_from_integer(number2);
//}
//
///*Multiplication and division here*/
//
//int64_t fp_mult(int64_t mult1, int64_t mult2)
//{
//	return ((int64_t)mult1)*mult2/FIXED_POINT; // nu ma incred in vreo conversie implicita
//}
//
//int64_t fp_mult_int(int64_t mult1, int64_t mult2)
//{
//	return mult1*mult2;
//}
//
//int64_t fp_div(int64_t mult1, int64_t mult2)
//{
//	return ((int64_t)mult1)*FIXED_POINT/mult2; // nu ma incred in vreo conversie implicita
//}
//
//int64_t fp_div_int(int64_t mult1, int64_t mult2)
//{
//	return mult1/mult2;
//}

