/*
 * fp-arith.h
 *
 *  Created on: Dec 25, 2015
 *      Author: osd
 */

#ifndef FPARITH_H_
#define FPARITH_H_


#endif /* FPARITH_H_ */
/*
 * prea incet, incerc cu macrouri
int64_t to_fp_from_integer (int64_t);
int64_t to_integer_from_fp_trunc(int64_t);
int64_t to_integer_from_fp_round(int64_t);

int64_t fp_add(int64_t, int64_t);
int64_t fp_sub(int64_t, int64_t);
int64_t fp_add_int(int64_t, int64_t);
int64_t fp_sub_int(int64_t, int64_t);

int64_t fp_mult(int64_t, int64_t);
int64_t fp_mult_int(int64_t, int64_t);
int64_t fp_div(int64_t, int64_t);
int64_t fp_div_int(int64_t, int64_t);
*/

#define FPOINT 1<<14
#define to_fp_from_integer(x) (x)*(FPOINT)
#define to_integer_from_fp_trunc(x) (x) / (FPOINT)
#define to_integer_from_fp_round(x) ((x) >= 0) ? ((x) + (FPOINT)/2) / (FPOINT) : ((x) - ((FPOINT)/2)) / (FPOINT)
#define fp_add_int(x, n) (x) + (n) * (FPOINT)
#define fp_sub_int(x, n) (x) - (n) * (FPOINT)
#define fp_mult(x, y) ((int64_t)(x)) * (y) / (FPOINT)
#define fp_div(x, y) ((int64_t)(x)) * (FPOINT) / (y)

