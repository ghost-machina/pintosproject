#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H
#include <list.h>

void syscall_init (void);


struct file_elem{ /* structure to associate fid with file*/
	int fid;
	struct file *file;
	struct list_elem global_elem; /*element in all_files_list;*/
	struct list_elem thread_elem; /* element in thread's list of
		opened elemets*/
};
#endif /* userprog/syscall.h */
