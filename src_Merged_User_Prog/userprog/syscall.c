#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include <list.h>

#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"




static void syscall_handler (struct intr_frame *);
static struct lock file_exec_lock;

static struct lock *lock_files; /* syncronize file processing */
static struct list all_files_list;
static struct file_elem *find_file(int);
static int global_fid = 2;


void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&file_exec_lock);

  lock_init(&lock_files);
  list_init(&all_files_list);
}


static int open(const char*  name)
{
	struct file* file;
	struct file_elem *f;
	int fid = global_fid++;

	f = (struct file_elem*)malloc(sizeof(struct file_elem));

	if(!is_user_vaddr(name))
	{
		return -1;
	}

	lock_acquire(&lock_files);
	file = filesys_open(name);
	f->file = file;
	f->fid = fid;
	list_push_back(&thread_current()->files, &f->thread_elem);
	list_push_back(&all_files_list,&f->global_elem);
	lock_release(&lock_files);
	return fid;
}

static int filesize(int fid)
{
	struct file_elem *f = find_file(fid);

	if (f == NULL)
		return -1;
	else
		return file_length(f->file);
}

static int read(int fid, void *buffer, unsigned size)
{
	int return_value,i;
	struct file_elem *f;

	if (!is_user_vaddr(buffer) || !is_user_vaddr(buffer+size))
	{
		return -1;
	}
	lock_acquire(&lock_files);

	if(fid == 0)
	{
		for(i=0;i<size;i++)
		{
			*((uint8_t*)buffer+i)=input_getc();
		}
		return_value = size;
	}
	else if(fid == 1)
	{
		return_value = -1;
	}
	else
	{
		f = find_file(fid);
		if (f == NULL)
		{
			lock_release(&lock_files);
			return -1;
		}

		if(file_tell(f->file) + size > file_length(f->file))
		{
			return_value = file_length(f->file) - file_tell(f->file);
		}
		else
		{
			return_value = size;
		}
		file_read(f,buffer,return_value);
	}

	lock_release(&lock_files);
	return return_value;
}

static int write(int fid, const void *buffer, unsigned size)
{
	int return_value;
	struct file_elem *f;

	if(!is_user_vaddr(buffer) || !is_user_vaddr(buffer+size))
	{
		return -1;
	}

	lock_acquire(&lock_files);
	if (fid == 1) /*console*/
	{
			putbuf(buffer,size);
			return_value = (int)size;
	}
	else if(fid == 0)
	{
		return_value = -1;
	}
	else
	{
		f = find_file(fid);
		if (f == NULL)
		{
			lock_release(&lock_files);
			return -1;
		}

		if(file_tell(f->file) + size > file_length(f->file))
		{
			return_value = file_length(f->file) - file_tell(f->file);
		}
		else
		{
			return_value = size;
		}
		file_write(f,buffer,return_value);
	}
	lock_release(&lock_files);
	return return_value;
}

static void seek(int fid, unsigned position)
{
	int newPos;
	struct file_elem *f = find_file(fid);

	if (f==NULL)
		return -1;

	if (position < 0 || position > file_length(f->file))
		return -1;

	file_seek(f->file,position);
}

static unsigned tell(int fid)
{
	struct file_elem *f = find_file(fid);

	if (f == NULL)
		return -1;

	return file_tell(f->file);
}

static void close(int fid)
{
	struct file_elem *f = find_file(fid);

	if(f != NULL)
	{
		file_close(f->file);
		list_remove(&f->global_elem);
		list_remove(&f->thread_elem);

		free(f);
	}
}

static struct file_elem *find_file(int fid)
{
	struct list_elem  *f_elem = list_begin(&all_files_list);

	while(f_elem != list_tail(&all_files_list))
	{
		struct file_elem *file = list_entry (f_elem, struct file_elem, global_elem);
		if(file->fid == fid)
		{
			return file;
		}
		f_elem = f_elem->next;
	}

	return NULL;
}


static void
syscall_handler (struct intr_frame *f )
{
	int syscall_no =((int*)f->esp)[0];
	char* arguments;

	int ptr_data;
	bool are_waiting = false;

	int fid;
	void *buffer;
	char *cbuffer;
	unsigned pos;

  switch(syscall_no) {
  /*
  case SYS_EXIT:
	  printf("SYS CALL EXIT!");
	  struct thread *current = thread_current();

	  thread_exit();
	  break;
  case SYS_WRITE:
	  printf("SYS CALL WRITE");
	  f->eax =0;
	  return;
	  break;
	  */
  case SYS_HALT:
	  printf("SYS CALL HALT");
	//  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 //	  if (is_user_vaddr((const void *) ptr_data)) {
	 		 shutdown_power_off();
	 //	  }
	  break;

  case SYS_EXIT:
	  printf("SYS CALL EXIT!");

	  ptr_data = (int *) f->esp + 1;
/*
	   //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 		  arguments[0] = *ptr_data;
	 	  }

*/
	 	 struct thread *current_thread = thread_current();
	 	 struct thread *child_iterator_thread;
	 	 struct list_elem *listEl;
	 	 // ne uitam sa vedem daca are procese care asteapta
	 	for (listEl = list_begin (&current_thread->children_processes); listEl != list_end (&current_thread->children_processes); listEl = list_next (listEl))
	 	{
	 		child_iterator_thread =  list_entry (listEl, struct thread, child);
	 		if (child_iterator_thread->status == THREAD_BLOCKED)
	 		{
	 			thread_unblock (child_iterator_thread); //altfel e deadlock
	 			are_waiting = true;
	 		}
	 		else
	 		{
	 		child_iterator_thread->parent_process = NULL;
	 		list_remove (&child_iterator_thread->child);
	 		}
	 	}


/*
	 	 while(!list_empty(&current_thread->children_processes))
	 	 {
				 child_iterator_thread = list_begin(&current_thread->children_processes);

				 if (child_iterator_thread->status == THREAD_BLOCKED)
					 thread_unblock (child_iterator_thread);
				else
				{

				list_remove (&child_iterator_thread,);
				}
				 if (thread_alive(&child_iterator_thread))
				 {
					 are_waiting = true;
				 }
	 	 }
*/
	 	//ne uitam sa inchided toate fisierele deschise penru thread-ul current altfel ar
	 	 // putea duce la erori neprevazute
	 	while (!list_empty (&current_thread->list_of_files))
	 	{
	 	listEl = list_begin (&current_thread->list_of_files);
			///close(file id din listEl);
	 	}

	 	if (are_waiting)
	 	{
		 	printf("Thread Name %s has exited with exit code %d. Has waiting children", current_thread->name, current_thread->status);
	 	}
	 	else
	 	{
		 	printf("Thread Name %s has exited with exit code %d. Has no waiting children", current_thread->name, current_thread->status);
	 	}

	 	current_thread->RETURN_STATUS_CODE = ptr_data;

	 	process_exit();
	 	//thread_exit();
	  break;
  case SYS_WAIT:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
//	  if (is_user_vaddr((const void *) ptr_data)) {
		 // arguments[0] = *ptr_data;
//	  }
	 int return_code_from_wait = process_wait(ptr_data); //apelam un process wait dupa process id
	 f->eax = return_code_from_wait;
	  break;

  case SYS_EXEC:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	  	  if (is_user_vaddr((const void *) ptr_data)) {
	  		void *ptr_page = pagedir_get_page(thread_current()->pagedir, ptr_data);
	  		if (ptr_page)
	  		{
	  			arguments = ptr_data;
	  		}
	  		else
	  		{
	  			//error ar trebui sa iesim cumva de aici ca inseamna
	  			// ca nu toate elementele au fost ok e ceva gen adresa de baza + buffers length
	  			// din cate am inteles din documentatia pintos.
	  			process_exit(-800);
	  		}
	  	  }

	  int return_code_from_exec ;
	  lock_acquire (&file_exec_lock);
	  return_code_from_exec = process_wait(process_execute (arguments));
	  lock_release (&file_exec_lock);
	  f->eax = return_code_from_exec;
	  break;

  case SYS_CREATE:

	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 		  arguments = ptr_data;
	 	  }
	 	 ptr_data = (int *) f->esp + 2; //luam de pe stiva arg[1],fiind numele fisierului, si verificam daca e pointer valid

	 	int return_code_from_create_file;
	 	lock_acquire(&file_exec_lock);
	 	return_code_from_create_file = filesys_create((const char *)arguments, (unsigned) ptr_data);
	 	lock_release(&file_exec_lock);
	 	f->eax = return_code_from_create_file;
	  break;
  	case SYS_REMOVE:
	  ptr_data = (int *) f->esp + 1; //luam de pe stiva arg[0],fiind numele exec, si verificam daca e pointer valid
	 	 	  if (is_user_vaddr((const void *) ptr_data)) {
	 	 		  arguments = ptr_data;
	 	 	  }

	 	 	int return_code_from_delete_file;
	 	 	lock_acquire(&file_exec_lock);
	 	 	return_code_from_delete_file = filesys_remove((const char *)arguments);
	 	 	lock_release(&file_exec_lock);
	 	 	f->eax = return_code_from_delete_file;
	  break;

	case SYS_OPEN:
			//f->eax = open(const char* file)
			cbuffer = (char *)((int *)f->esp)[1];
			f->eax = open(cbuffer);
			break;
		case SYS_FILESIZE:
			//f->eax = filesize(int fid)
			fid = (int)((int *)f->esp)[1];
			f->eax = filesize(fid);
			break;
		case SYS_READ:
			//f->eax = read(int,void,unsigned)
			fid = (int)((int *)f->esp)[1];
			buffer = (void *)((int *)f->esp)[2];
			pos = (int)((int *)f->esp)[3];
			f->eax = read(fid,buffer,pos);
			break;
		case SYS_WRITE:
			//f->eax = write(int,const void *buffer, unsigned size)
			fid = (int)((int *)f->esp)[1];
			buffer = (void *)((int *)f->esp)[2];
			pos = (int)((int *)f->esp)[3];
			f->eax = write(fid,buffer,pos);
			break;
		case SYS_SEEK:
			//seek(int fid,unsigned position)
			fid = (int)((int *)f->esp)[1];
			pos = (int)((int *)f->esp)[2];
			seek(fid,pos);
			break;
		case SYS_TELL:
			//f->eax = tell(int);
			fid = (int)((int *)f->esp)[1];
			f->eax=tell(fid);
			break;
		case SYS_CLOSE:
			//close(int);
			fid = (int)((int *)f->esp)[1];
			close(fid);
			break;
		default:
		    NOT_REACHED ();
	}	
}
